# Puce RFID

L’utilisation d’une puce RFID utilisant le réseau LORA pour « Long Range » nous permet de localiser via des ondes radios longues portées (supérieur à 100m). De plus en plus utilisé dans l’IOT « Internet Of Things » - représentant les objets connectés – car elles permettent, par-exemple la transmission simple de données de localisation, notamment dans le cas de marquage contre le vol ou encore dans l’industrie afin de localiser des personnes notamment dans les cas de personnes travaillant sur de grandes zones.

Dans notre cas, l’utilisation d’un tel dispositif nous permet deux choses :

1.	Bénéficier d’un objet à la taille réduite permettant de localiser notre vélo sans forcément avoir besoin d’avoir recours aux réseaux mobiles GSM afin de faire transmettre les données enregistrées par la puce.
2.	La puce RFID n’a besoin que de très peu d’énergie pour fonctionner, elle n’est pas énergivore ce qui représente un avantage dans un système de marquage antivol. Devoir recharger une batterie sans arrêts seraient problématique dans le cas d’un dispositif censé empêcher les vols.

Notre système serait basé sur l’utilisation d’une puce RFID dîtes « ACTIVE », bien que nécessitant une source d’énergie, elle permet de diffuser un signal en continu et ce dans un rayon de 150m sans avoir recours à aucun système de transmission. Au-delà, c’est à cet instant que le réseau LORA peut-être un atout précieux permettant d’étendre la portée de la puce, mais il existe également d’autre dispositifs...
