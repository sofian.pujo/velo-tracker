## Batterie Lithium

A la différence de l’utilisation d’une simple pile pour l’alimentation du dispositif, l’utilisation d’une batterie nous permettrait de pouvoir alimenter tout le dispositif de façon plus pérenne sur le long terme. De plus, les batteries au lithium-ion contrairement aux piles classiques ont l’avantage de pouvoir être plate ce qui permet d’optimiser l’utilisation de l’espace au sein du boitier afin d’éviter que celui ne soit trop imposant ce qui pourrait nuire à la vocation même de notre produit.

L’utilisation d’une batterie au lithium-ion nous permettra d’alimenter de façon active l’Arduino ainsi que la puce RFID afin de maximiser sa portée et permettre au besoin de transmettre plus de données sans être limité par le débit et la portée qui est relativement moyenne avec une puce RFID dites « PASSIVES ».

Facilement rechargeable et d’un coût relativement bon marché, elles permettront d’alimenter le produit sans réel contrainte pour l’utilisateur : celui-ci devra seulement recharger la batterie contenue dans le boitier afin de conserver toutes les facultés de la puce RFID.

Rappelons également que pour fonctionner l’Arduino nécessite au minimum 3.3v mais les performances seraient dégradées et il en résulterait une bande passante moindre pour la transmission des données récoltées par la puce RFID. Une batterie de ce type permettrait une utilisation nomade, de longue durée afin de ne pas être pénalisante sur l’utilisation du dispositif antivol : la batterie n’aura alors pas besoin d’être constamment rechargé pour pouvoir faire fonctionner notre produit.