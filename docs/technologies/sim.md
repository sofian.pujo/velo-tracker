# Carte SIM M2M

L’ajout d’une carte de SIM M2M permet de transmettre les données de la puce RFID vers une plateforme web, ou un serveur applicatif. Cela permet également de combiner les connectivités afin d’obtenir une couverture optimale. Les puces RFID qui communique via le réseau de télécommunication radio longue portée « LORA » sont reliées à des bases STATION ou GATEWAY ayant une portée de 10 à 20km.

Ces cartes SIM sont spécialement dédiées à être utilisé dans des objets connectés afin de faire transiter des informations sur un serveur et ce, qu’elle que soit la distance séparant l’émetteur du récepteur de l’information. Cela représente un avantage pour garantir une sécurité supplémentaire pour notre produit.
Disponible dans une multitude de formats comme les cartes SIM classiques et déployable à l’internationale elles s’intégreront facilement dans notre boitier via l’utilisation d’un module GSM spécifiquement conçu pour s’intégrer sur une carte Arduino.

L’utilisation de cette technologie supplémentaire nous permettra de garantir la transmission des données collectées depuis la puce RFID vers des serveurs distants. Combiner ainsi les connectivités nous permet d’assure une couverture réseau optimal de notre produit.