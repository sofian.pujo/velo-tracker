# Arduino

Arduino est une marque qui produit des cartes électroniques matériellement libres sur lesquelles on retrouve un microcontrôleur. De conception simple, il possède un régulateur linéaire en 5v ce qui est un avantage lorsque l’on veut produire un produit consommant très peu d’énergie afin de maximiser son utilisation sur le long terme en équipant le module d’une batterie lithium-ion.

L’un des principaux avantages concernant la conception du futur projet concerne sa taille. En effet, certains modèles disponibles tel que le « Nano » mesurant - 68mm x 53,3mm - ou encore le « Micro » qui lui mesure - 48mm x 18mm - ce qui est important dans la mesure où notre produit se doit d’être le plus petit possible afin de pouvoir être installer facilement sur n’importe quel vélo.

La seule contrainte qu’apporte l’utilisation de ce type de carte microcontrôleur réside dans la programmation en C/C++ car l’Arduino par sa petite taille n’embarque avec lui qu’une quantité limitée de mémoire ce qui oblige le développeur à prendre en compte tous ces aspects lors de la conception de son programme.

Dans notre cas, c’est la solution idéale, notre futur produit n’a besoin que d’envoyer des informations sommaires (position géographiques, heures, etc.)
