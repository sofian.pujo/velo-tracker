Velo Tracket
############

Vous trouverez ici une documentation sur plusieurs éléments de notre projet Velo Tracker.

Membres du projet :

*	Corentin Assemat – Chef de Projet (I1)
*	Sofian Pujo (I1)
*	Mickaël Sache (I1)
*	Sébastien Malmasson (I2)
*	Leonard Pasquier (I2)
*	Hugo Severin (I2)

Liens utiles
############

.. toctree::
    :maxdepth: 2

    technologies
    iot


Description du projet
#####################
L’objectif du projet « Vélo Tracker » et de pouvoir localiser notre vélo en cas de vol. Les utilisateurs possédant des vélos équiperont une balise qui serait reliée à leur smartphone via une application. 
De nos jours les vélos sont de plus en plus sensibles aux vols, cette balise permettra donc de traquer et de localiser un vélo afin que des autorités compétentes puissent le récupérer. Un second objectif de cette balise serait d’utiliser l’application mobile pour enregistrer les déplacements du vélo afin de pouvoir consulter les différents itinéraires réalisés lors de sortie. Ces itinéraires pourraient permettre de planifier des randonnées ou bien tout simplement de consulter la distance parcourue ainsi que d’autres statistiques.

Concept de base
***************
On positionnera la balise sous la selle du vélo afin qu’elle soit discrète, puis on conseille d’ajouter une sacoche de selle pour mieux la camoufler.

.. image:: img/balise.png

Ensuite, une application mobile nous permettra d’interagir avec cette balise pour notamment obtenir la position de la balise et son niveau de charge.