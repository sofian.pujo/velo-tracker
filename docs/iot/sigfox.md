# SigFox

Créé en 2009 par Christophe Fourtet et Ludovic Le Moan, SigFox est un opérateur réseau Français qui est aujourd’hui répandu tout autour de la planète. Il émet sur la bande des 868MHz.

## La documentation

Le site d’arduino présente une bibliothèque complète, avec seulement deux exemples de cas d’utilisation, mais plusieurs fonctions détaillées.

## La couverture disponible

Avec plus de 1300 antennes en France en 2014, il contient quand même plusieurs zones blanches (des zones sans réseau), notamment dans la moitié sud du pays.

![couv-sigfox](../img/couv_sigfox.png)

A l’internationale, SigFox se repose sur des antennes d’opérateurs téléphoniques pour étendre son réseau, lui permettant ainsi de se déployer dans les plus grands pays du monde.

## Le tarif

Un abonnement d’un an au réseau sigfox coûte une quarantaine d’euros. Ce à quoi il faut rajouter le module pour arduino, qui lui coûte une soixantaine d’euros. Nous arrivons donc à une centaine d’euros d’investissement nécessaire.

## La sécurité

Le site web de SigFox présente plusieurs points forts de son système en matière de sécurité : une interface en HTTPS, un système d’émission d’ondes avec fréquence variable permettant de résister aux interférences, une communication qui se fait uniquement quand l’émetteur le souhaite permettant ainsi de limiter la détection d’ondes de sigfox par les éventuels hackers, et leur laissant juste une toute petite fenêtre temporelle pour essayer de détourner le système. Enfin, SigFox a mis en place sa Hacking House : un lieu ou des étudiants et professionnels de l’informatique se retrouvent et tentes de hacker différents objets IoT, permettant ainsi de faire avancer la sécurité dans le domaine.

## Conclusion

En conclusion, SigFox est un système complet, utilisable tout autour du globe, et sécurisé, qui sur le papier a comme seul gros défaut sa couverture en France qui laisse à désirer.