# Cellulaire

Pour l’éventualité de l’utilisation du réseau cellulaire, nous nous baserions sur la solution de Arduino, Arduino SIM. 

## La documentation

Ce projet étant porté par Arduino, il contient une documentation complète sur leur site.

## La couverture disponible

Plus de 100 pays sont couverts par le réseau GSM. Malheureusement, nous ne trouvons pas de carte permettant de voir la couverture réseau de Arduino SIM en France.

## Le tarif

La puce à rajouter au Arduino, la GSM MKR 1400 coûte soixante à soixante-dix euros selon le site ou on la trouve. A ça, il faut rajouter un abonnement à un dollar cinquante par mois.
Sur un an, cela reviendrait à environ quatre-vingt-dix euros.

## La sécurité

La sécurité du service est simplement celle du réseau GSM standard.

## Conclusion

En conclusion, le système GSM peut paraître bien mais il est un peu cher, et on manque d’information sur la couverture réseau.