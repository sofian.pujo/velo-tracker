# Lora

LoRaWAN, pour Long Range Wide Area Network, est un protocole de communication se reposant sur la bande des 868MHz en Europe. Créé lui aussi en 2009, son entreprise fait partie de la LoRa Alliance, un rassemblement de plusieurs grandes entreprises ayant pour but de standardiser le LoRa et garantir son interopérabilité avec d’autres objets de L’IoT.

## La documentation

La technologie LoRa WAN étant énormément utilisée, internet regorge de tutoriels différents sur la façon de la mettre en place.

## La couverture disponible

Le réseau LoRa ne nécessite pas forcément d’abonnement pour être exploitable, puisqu’il est basé sur une fréquence libre. En revanche, pour pouvoir l’utiliser gratuitement, il faut pouvoir placer ses propres passerelles dans la zone d’utilisation du produit. 
Puisque nous cherchons à fabriquer un tracker pour vélo qui sera potentiellement utilisé à travers tout le pays, il serait extrêmement compliqué et onéreux d’aller de ville en ville placer nos propres passerelles afin de fonder notre propre réseau privé LoRa. C’est pour cette raison que nous faisons le choix de partir sur un abonnement.
Dans le monde des abonnements LoRa, il existe en France plusieurs acteurs, mais celui dont nous allons étudier la couverture est Orange, un membre de la LoRa Alliance.

![couv-lora](../img/couv_lora.png)

On remarque sur cette carte que le réseau LoRa est disponible presque partout en France, excepté vers Troyes ou on peut voir une petite zone blanche.
Puisque nous nous basons sur un système d’abonnement à un opérateur, il faudrait prendre un autre abonnement pour accéder au réseau LoRa d’un autre pays. Mais notre marché visé étant la France, ce point ne pèsera pas dans la balance.

## Le Tarif

L’abonnement coûte 1€ par mois pendant 36 mois chez Orange. 2€ si nous choisissons le forfait sans engagement. La carte de communication coûte elle une cinquantaine d’euros. Sur un an, l’utilisation revient donc à 75€ environ, si nous prenons le forfait sans engagement.

## La sécurité

Bien qu’utilisant un protocole  de chiffrement (l’AES128), le LoRa n’est pas infaillible. En effet, une étude de 2017 de JungWoon Lee montre qu’il est possible lors de l’initialisation de la communication entre l’émetteur et l’antenne relais de s’interposer et donc de faire croire à l’antenne relai qu’on est l’émetteur. Mais dans la même étude il propose une solution pour remédier à ce problème, que nous mettrons en place si nous choisissons cette technologie.

## Conclusion

LoRaWAN est une solution qui présente une très bonne couverture en France en plus d’être peu onéreuse. Elle démontre qu'aucun système n’est parfait avec sa faille de sécurité, mais elle est corrigeable et a permis aux équipes de LoRaWAN de sécuriser encore plus l’ensemble de leur système.