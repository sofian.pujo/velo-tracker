# The Things Network

Afin de pouvoir utiliser le réseau Lora, il était nécessaire d'enregistrer notre application sur le site internet : https://console.thethingsnetwork.org/

Nous avons donc enregistré norte application "Velo tracker".

![ttn](../img/ttn_application.png)

Une fois l'application enregistrée, nous devions également enregistrer notre équipement :

![ttn](../img/ttn_devices.png)

![ttn](../img/ttn_devices_2.png)

![ttn](../img/ttn_devices_3.png)

Une fois notre équipement enregistré, il fallait dans notre code Arduino renseigner ces éléments comme ceci au début de notre programme :

```c++
static const PROGMEM uint8_t NWKSKEY[16] = { 0x1D, 0x91, 0x42, 0x51, 0x39, 0x23, 0x77, 0x61, 0xBF, 0x93, 0x53, 0xD2, 0x49, 0xFC, 0x19, 0xB3 };

static const uint8_t PROGMEM APPSKEY[16] = { 0x1C, 0x0A, 0xB8, 0x5F, 0x8F, 0x09, 0xA1, 0x82, 0xD2, 0x79, 0xDF, 0x66, 0x35, 0x56, 0xB4, 0x2B };

static const uint32_t DEVADDR = { 0x26011302 };
```