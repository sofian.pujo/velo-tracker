IOT (Internet Of Things)
########################

| Pour le tracker de vélo que nous cherchons à fabriquer, un composant important à prendre en considération et le système de communication réseau entre le tracker, et le serveur contenant le site de suivi.
| Nous allons donc faire une comparaison des différents acteurs du marché, en se basant sur 5 critères :

* La documentation
        * En effet, une technologie peut être très bien faite, si il n’existe aucune documentation nous apprenant à l’utiliser, elle ne sera pas exploitable.
* La couverture disponible
        * Il serait dommage de choisir un système réseau qui ne couvre que la Russie, alors que le marché visé est la France.
* Le tarif
		* Que ce soit un abonnement, ou un paiement en une fois.
* La sécurité
        * Si une personne vole le vélo, elle ne doit pas pouvoir falsifier le signal envoyé par le tracker.

Pour ce comparatif, nous étudierons exclusivement des systèmes de type LPWAN : des réseaux sans fil à basse consommation. Nous exclurons donc le WiFi, le bluetooth, le Zigbee,...
Les acteurs principaux que nous allons étudier sont le SigFox, Lora, et le réseau cellulaire.


.. toctree::
    :maxdepth: 2

    iot/sigfox
    iot/lora
    iot/cellulaire
    iot/thethingsnetwork

Conclusion
**********
Le système nous paraissant comme étant le plus adéquat par rapport à notre besoin est le système LoRaWAN. En effet, il présente l’avantage d’être économique, bien documenté, sécurisé, et a une couverture plus que satisfaisante sur l’ensemble du territoire français.
